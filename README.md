# StageVerslagen


## DagVerslag Maandag 2018-10-01

Vandaag ben ik aangekomen in het gemeentehuis van Zandhoven, waar Herman me met open armen heeft ontvangen.
Ik ben er direct ingevlogen en ben gestart met harde schijven op te vijzen en de disks uit te halen om
later te vernietigen. Vervolgens zijn we koffie gaan drinken en hebben we wat over de IT-wereld in het 
algemeen gepraat. Eens terug boven ben ik gestart een pc te booten van een USB waar een windows 7 Pro opstond,
Om terug geherinstalleerd te worden. Dan heb ik heel veel opgezocht over de MDT en ADK totdat ik versies had
gevonden die compatibel zijn met elkaar en met windows 7. Van hieruit heb ik op de pc een sysprep.exe uitgevoerd,
dan heb ik alles met MDT voorbereid om een referentie PC te maken.


## DagVerslag Dinsdag 2018-10-02

Ik heb alles weer overlopen en ben begonnen met de MDT te checken eens dat dit gebeurt was , heb ik
van de andere pc een image gemaakt via een commando dat verbinding maakte met MDT server en dan de
deployment van de windows 7 gedaan. Eens dat dit afgerond was ben ik begonnen aan dezelfde opdracht maar
dan voor een windows 10. We dachten dat dit eenvoudiger ging zijn, maar we krijgen al uren aan een stuk
verschillende foutmeldingen. Hierdoor heeft Herman besloten me een andere opdracht te geven en waarschijnlijk 
het deployment uitstellen naar een andere dag. De nieuwe opdracht lijkt me zeer interessant en moet ik 
natuurlijk mijn hoofd bij gebruiken,kortom mag ik even architect spelen. En de bekabeling en de arranging
van alles in 3D Paint of ergens anders ter realisatie brengen.


## Dagverlsag Woensdag 2018-10-03

We zijn 's morgens vroeg begonnen met het opmeten van een gebouw, zodat ik nadien een Visio
plattegrond kon maken, na de afmetingen heb ik wat geëxperimenteerd met Visio. Ik kwam er al
snel achter dat Visio een goed maar redelijk gecompliceerde applicatie is. Het was de eerste
dag dat we compleet waren Marcin was vandaag ook gekomen. We zijn een geluidsinstallatie en 
een beemer gaan installeren dit duurde enkele uren.


## Dagverslag Donderdag 2018-10-04

Ik ben begonnen met de plattegrond in Visio te verfijnen, wanneer dit klaar was zijn we 
terug naar het zaaltje moetten gaan omdat er problemen waren, het was een heel simpel
prbleem. ze wisten niet hoe ze de subtitle naar nederlands moesten zetten, dus dit was snel 
opgelost. Ik ben ook terug begonnen met de deployment server voor een Windows 10 te capturen.
En ben bezig met een verslag: https://docs.google.com/document/d/1tWOPM0xz-NEIrR5aLKAE1CI9tly56FGGGaZSSb8bvow/edit 

## Dagverslag Vrijdag 2018-10-05

We hebben vandaag geen stage omdat Herman wegens bepaalde redenen niet aanwezig kan zijn.
Ik heb thuis nog gewerkt aan de gip-taken. Ik vind dit zelf heel spijtig omdat ik dit 
een heel leerrijke stage vond en ik had graag de vrijdag ook aanwezig geweest en nog een 
extra taak gekregen.